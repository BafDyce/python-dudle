class DudleUrl(object):
    def __init__(self, id):
        self.url = self.__create_dudle_url(id)

    def __create_dudle_url(self, id):
        if id.startswith('http://'):
            if id.startswith('http://dudle.inf.tu-dresden.de/'):
                # Force https for official dudle site
                return id.replace('http://', 'https://')
            return id
        elif id.startswith('https://'):
            return id
        else:
            return 'https://dudle.inf.tu-dresden.de/' + id + '/'

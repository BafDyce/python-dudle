import enum
import termcolor

class DudleDecision(enum.Enum):
    UNKNOWN = 'unknown'
    YES = 'yes'
    NO = 'no'
    MAYBE = 'maybe'

    def __str__(self):
        return self.value

    def colored(self, format='{}'):
        fg = 'grey'
        bg = 'default'
        if self == DudleDecision.YES:
            fg = 'grey'
            bg = 'on_green'
        elif self == DudleDecision.NO:
            fg = 'grey'
            bg = 'on_red'
        elif self == DudleDecision.MAYBE:
            fg = 'grey'
            bg = 'on_yellow'

        return termcolor.colored(format.format(self.value), fg, bg)

from bs4 import BeautifulSoup
import requests

from .decision import DudleDecision
from .participant import DudleParticipant
from .url import DudleUrl

class DudleTable(object):
    def __init__(self, url):
        (options, participants) = DudleTable.__parse_dudle(url)
        self.options = options
        self.participants = participants

    def print_ascii(self):
        print(' ' * 20, end='')
        for option in self.options:
            print('|{:^12s}'.format(option), end='')
        print('|')

        for participant in self.participants:
            print('{:>19s} '.format(participant.name), end='')
            for decision in participant.decisions:
                print('|', decision.colored(format='{:^12s}'), sep='', end='')
            print('|')

    def as_html_table(self, colored=True):
        colwidth = max([len(option) for option in self.options]) + 2
        colformat = '{:^' + str(colwidth) + 's}'

        from dominate.tags import table, tbody, tr, td, p, style
        from dominate.util import raw, unescape
        tt = table()
        tt.add(style(raw(
        """
        td.maybe { text-align:center; color:black; background-color: orange; }
        td.yes { text-align:center; color:black; background-color: green; }
        td.no { text-align:center; color:black; background-color: red; }
        """)))
        with tt.add(tbody()):
            header = tr()
            header.add(td(''))
            for option in self.options:
                header.add(td(option))

            for participant in self.participants:
                row = tr()
                row.add(td(participant.name))
                for decision in participant.decisions:
                    row.add(td(colformat.format(decision), cls=decision))

        return str(tt)


    def __parse_dudle(url):
        assert(isinstance(url, DudleUrl))
        res = requests.get(url.url)

        if res.status_code == 200:
            soup = BeautifulSoup(res.content, 'html.parser')
            table = soup.find(id='participanttable')
            options = list(map(
                lambda item: item.find('a').contents[0],
                table.find_all('th', title='')
            ))[1:-1]

            def parse_participantrow(participantrow):
                entries = participantrow.find_all('td')[1::]
                participant = None
                for entry in entries:
                    if 'name' in entry['class']:
                        participant = DudleParticipant(entry.find('span').contents[0])

                if participant:
                    for entry in entries:
                        classes = entry['class']
                        if 'vote' in classes:
                            if 'a_yes__' in classes:
                                participant.add_decision(DudleDecision.YES)
                            elif 'b_maybe' in classes:
                                participant.add_decision(DudleDecision.MAYBE)
                            elif 'c_no___' in classes:
                                participant.add_decision(DudleDecision.NO)
                            else:
                                participant.add_decision(DudleDecision.UNKNOWN)

                return participant

            participants = list(map(
                lambda participantrow: parse_participantrow(participantrow),
                table.find(id='participants').find_all('tr', 'participantrow')
            ))

            return (options, participants)

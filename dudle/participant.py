class DudleParticipant(object):
    def __init__(self, name):
        self.name = name
        self.decisions = []

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        return self.name + ": " + str(self.decisions)

    def add_decision(self, decision):
        self.decisions.append(decision)

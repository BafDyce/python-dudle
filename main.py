#!/usr/bin/env python3
import argparse
import dudle

# Planned TODO for v0.1:
# - support anonymous polls
# - customizable colors

def main():
    parser = argparse.ArgumentParser(description='Display dudle polls')
    parser.add_argument('poll', help='id or full url of the dudle poll')
    parser.add_argument('--html', action='store_true', help='Generate HTML code of table')
    args = parser.parse_args()

    url = dudle.DudleUrl(args.poll)
    table = dudle.DudleTable(url)
    if args.html:
        print(table.as_html_table())
    else:
            table.print_ascii()


if __name__ == '__main__':
    main()
